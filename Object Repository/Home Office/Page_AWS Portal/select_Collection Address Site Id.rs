<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Collection Address Site Id</name>
   <tag></tag>
   <elementGuidId>79d46ba5-1028-401c-aa3d-6d6772627fec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class=' css-2b097c-container'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-6</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Collection Address Site Id : *    option -- Select -- focused, 1 of 60. 60 results available. Use Up and Down to choose options, press Enter to select the currently focused option, press Escape to exit the menu, press Tab to select the option and exit the menu.-- Select ---- Select --35Aws-Collection1KlCollectionIndia-90902-1Collection9090New242India-Hyderabad-1jk001TES India @NewTES SingaporeIndia-123432-1Collection9090-234-id1India-123489-1New Collection 1TES IND@21Collection 12334Aws-CollectionSiteCollectionMadhu-trueCollection890klCollection90901tyunew site Id 1-trueSD456HMadhuCollection 78India-Hyderabad-2UV4GUDEGTest MadyCollection123THOGATAPALEMMadhu7889collectionNewCol1Home Customer Collection Site-trueIndia-1234-id1Madhu56Madhu21Collection678TES INDVHT4GYTIndia-1234-2collection-home-1TES India@ 18-05-21jk001-234-1Madhu345644aws789jk Collection-Hyderabad-1India-1234-3test mady-Hyd-1India-Hyderabad1-2Collection site32collection1India-1234-1India-9090-1AWS</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;flex-container&quot;]/form[@class=&quot;form av-invalid&quot;]/div[@class=&quot;userDetailsContainer&quot;]/div[@class=&quot;inner&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;user-form row&quot;]/div[@class=&quot;col-xl-6&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/form/div/div/div/div/div/div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]</value>
   </webElementXpaths>
</WebElementEntity>
