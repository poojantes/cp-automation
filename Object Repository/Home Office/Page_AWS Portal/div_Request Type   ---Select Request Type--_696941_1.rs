<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Request Type   ---Select Request Type--_696941_1</name>
   <tag></tag>
   <elementGuidId>5262dda0-72a3-447c-8191-6af0f51e0a5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.user-form.row</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/form/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>user-form row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Request Type : * ---Select Request Type---Prepaid label for drop-offQR code for drop-offPrepaid label including collectionEquipment Type : * AccessoriesDesktop ComputersLaptopsTabletsPackagingsBrand Name : * MicrosoftGoogleWeight incl. packaging (kg) : *Collection Address Site Id : * Aws-Collection1Surname : *First Name : *Street/Road : *House Name/No : *Mobile Number : *Post Code : *Town/City : *Country : * IndiaEmail Address : *Collection Date : *June, 2021SunMonTueWedThuFriSat3031123456789101112131415161718192021222324252627282930123456789102021JanFebMarAprMayJunJulAugSepOctNovDec2020 - 2029201920202021202220232024202520262027202820292030Collection from : 08:0009:0010:0011:0013:0014:0015:0016:0017:0018:0019:0020:00Collection untill : 08:0009:0010:0011:0013:0014:0015:0016:0017:0018:0019:0020:00</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;flex-container&quot;]/form[@class=&quot;form av-valid&quot;]/div[@class=&quot;userDetailsContainer&quot;]/div[@class=&quot;inner&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;user-form row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/form/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back To Request Summary'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return request form'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
