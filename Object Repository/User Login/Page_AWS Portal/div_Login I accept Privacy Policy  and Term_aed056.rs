<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login I accept Privacy Policy  and Term_aed056</name>
   <tag></tag>
   <elementGuidId>f954e7ea-17fe-4f7a-9021-7d031b6cedaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body.text-center</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Login I accept Privacy Policy  and Terms LoginForgot password? ResetSSO Sign In' or . = 'Login I accept Privacy Policy  and Terms LoginForgot password? ResetSSO Sign In')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body text-center</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login I accept Privacy Policy  and Terms LoginForgot password? ResetSSO Sign In</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;auth-wrapper&quot;]/div[@class=&quot;auth-content&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body text-center&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
