<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Select --ADF KITAll-in-oneAssBeze_33d804</name>
   <tag></tag>
   <elementGuidId>1382b1d7-7e76-46f9-a9ca-c55a0f465c71</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[12]/following::select[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.asset-row.row > div.form-group > select.select-css</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select-css</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> -- Select --ADF KITAll-in-oneAssBezelBTCPU Cooler FanDockStationDVDVIFanHardDriveIpad AWSkitLAPLarge KeyboardLTPmonitorNew Asset TypeNICOPOppoSwitchSwitch ControlTabletTDTestTest CheckUpsUSBUSB cableUSB Cable MiniWorkStation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog my-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;padding-unset modal-body&quot;]/div[1]/div[@class=&quot;add-asset-container&quot;]/div[@class=&quot;asset-dialog-body card&quot;]/div[@class=&quot;asset-row row&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;select-css&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[12]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[13]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div/div/div/div/select</value>
   </webElementXpaths>
</WebElementEntity>
