<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Customer PO123Cust Reference NumberClie_ddd938</name>
   <tag></tag>
   <elementGuidId>275da70e-f84d-4d3e-ac80-5728c492755e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.no-shadow.card</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>no-shadow card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Customer PO:123Cust Reference Number:Client Entity:New Drop Down:test:testNew Check:Circle/Epicor Order Number :*User Name:EshaSITPortal Name:AWS PortalClientAWS Portal-INDIA Request Raised by:Name:Esha SIT Email:eshadaipule@gmail.com Mobile No : +919090909090  Processing Partner Processing Partner -- Select --Collection Site-TES India-AWSLoading collection address...Collection Site Information:Truck Size :Truck Length :--Dock Hours From :Dock Hours To :Fork lift Required :NoCertificate Of Insurance Required :NoLift Gate Required :NoSecurity Clearance Required :NoUnion Labor :No Request Contact Information PrimaryName:Esha SITMobile No:+91 9090909090E-mail IDeshadaipule@gmail.com Box Info Detail Box Count:2Pallet Count:1Pallet Weight:50Asset and ServicesServicesService CostAsset TypesAsset QuantityTotal CostTotal Services Cost:$ 0 Reports &amp; Documents  Request DocumentsTypeNameTES UserActionAsset List DocumentAssetList_2147.xlsxNo--Select List--TES-USAInvoice RequestNew Request Document Customer ReportsTypeNameTES UserActionPlease update Circle/Epicor Order number to upload customer report. The Circle/Epicor number should be TES ST No. from customer reports.PO Details PO Number-- Select --A1GA2GA3GAIGAWSAWSAWS2AWS3AWS-IndiaAWS-USASalesforcetest123Processing FeeFreight FeeTotal FeeAssign PO</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;app-container app-theme-white fixed-header fixed-sidebar fixed-footer sidebar-mobile-open&quot;]/div[@class=&quot;app-main&quot;]/div[@class=&quot;app-main__outer&quot;]/div[@class=&quot;app-main__inner&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;px-3 py-4&quot;]/div[@class=&quot;requ-info&quot;]/div[@class=&quot;requ-details&quot;]/div[@class=&quot;no-shadow card&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Request Details'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Processing Done'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
