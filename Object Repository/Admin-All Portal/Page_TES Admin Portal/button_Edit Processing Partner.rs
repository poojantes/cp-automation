<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Edit Processing Partner</name>
   <tag></tag>
   <elementGuidId>659c6c0b-34f7-4837-8f46-87b4cc1c6956</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@class='mr-1 btn btn-primary btn-sm' and @title='Edit'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
