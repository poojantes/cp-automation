<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Raise Request Testing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d1aa9212-48bc-499c-bbe2-07bb35bd25cd</testSuiteGuid>
   <testCaseLink>
      <guid>27b9eac9-8df8-4e2d-b001-3681cafd2e74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Request Types/Raise_TES Pack and Ship Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a15bd6a-6d04-44cb-95a5-84d90a64d4ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Request Types/Update Quote_TES Pack and Ship Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a322c318-4bfa-4481-886b-8f0ffcdd009d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Request Types/Process_TES Pack and Ship Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5254e6f8-caac-448f-a311-9671424ce7b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Request Types/Approve_TES Pack and Ship Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1652b295-c8ff-4f6d-a6a3-625adcd352bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Request Types/Raise_Client Packed Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83242475-e346-46f8-91c4-f4d018bd5224</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Request Types/Process_Client Packed Request</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
