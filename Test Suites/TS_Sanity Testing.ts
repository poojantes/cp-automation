<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Sanity Testing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>feaee10e-b4b2-4181-9ef6-378241e75fcb</testSuiteGuid>
   <testCaseLink>
      <guid>36a4fe39-5fe2-4916-bd70-8e8ad83f6987</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sanity Test Cases/Admin Sanity Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abecd4b5-4f67-4775-961a-a1ca4558bc5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sanity Test Cases/User Sanity Test</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
