package homeOffice
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class Login {
	@Given("I open the Browser and navigate to the URL")
	public void i_open_the_Browser_and_navigate_to_the_URL() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()

		WebUI.navigateToUrl(GlobalVariable.HomeOfficeURL)

		WebUI.deleteAllCookies()
	}

	@When("I enter Valid Login Email")
	public void i_enter_Valid_Login_Email() {
		WebUI.setText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input_Login_email'), GlobalVariable.Email)
	}

	@When("Click Request Login OTP")
	public void click_Request_Login_OTP() {
		WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Request Login OTP'))
		WebUI.delay(1)
	}
	
	@Then("verify text OTP expired, click on Resend OTP to generate new OTP")
	public void verify_OTPExpiry() {
		WebUI.verifyTextPresent('OTP expired, click on Resend OTP to generate new OTP.', false)
	}

	@Then("verify text Your OTP is sent to your email successfully.")
	public void verify_text_OTP() {
		WebUI.verifyTextPresent('Your OTP is sent to your email successfully.', false)
	}

	@Then("Enter the OTP and accept Privacy Policy")
	public void enter_the_OTP_and_accept_Privacy_Policy() {
		WebUI.delay(25)
		WebUI.click(findTestObject('Home Office/Page_AWS Portal/input_Login'))
	}

	@When("click Login button")
	public void click_Login_button() {
		WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Login'))

		WebUI.delay(2)
	}

	@Then("verify Username")
	public void verify_Username() {
		
		WebUI.waitForElementPresent(findTestObject('Home Office/Page_AWS Portal/div_Username'), 5)
		WebUI.verifyElementPresent(findTestObject('Home Office/Page_AWS Portal/div_Username'), 1)
		
		WebUI.delay(5)
	}

	@And ("wait Request timeout")
	public void wait_Request() {
		WebUI.delay(180)
	}

	@When("I Click Username")
	public void i_Click_Username() {
		WebUI.click(findTestObject('Home Office/Page_AWS Portal/div_Username'))
	}

	@When("Click Logout")
	public void click_Logout() {
		WebUI.click(findTestObject('Home Office/Page_AWS Portal/span_Logout'))
	}

	@Then("verify title as Login")
	public void verify_title() {
		WebUI.verifyTextPresent('Login', false)
	}

	@When("I enter Invalid Login Email")
	public void i_enter_Invalid_Login_Email() {
		WebUI.setText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input_Login_email'), 'test@test.com')
	}

	@Then("I verify text Your not authorized user.Contact your Admin.")
	public void i_verify_textnotAuth() {
		WebUI.verifyTextPresent('Your not authorized user.Contact your Admin.', false)
	}

	@When("Click Resend OTP button")
	public void click_Resend_OTP_button() {
		WebUI.click(findTestObject('Home Office/Page_AWS Portal/button_Resend OTP'))
	}

	@Then("Enter the Invalid OTP and accept Privacy Policy")
	public void enter_the_Invalid_OTP_and_accept_Privacy_Policy() {
		WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input_OTP'),'0000')
		WebUI.click(findTestObject('Home Office/Page_AWS Portal/input_Login'))
	}

	@Then("verify text as Invalid OTP, Please enter valid OTP")
	public void verify_text_as() {
		WebUI.verifyTextPresent('Invalid OTP, Please enter valid OTP', false)
	}
}