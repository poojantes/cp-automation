import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.And
import cucumber.api.java.en.When



class AdminUserCreation {

	@Given("I navigate to Admin Users")
	def navigate_To_Admin_User() {
		WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(2)

		WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Master Data'))

		WebUI.scrollToElement(findTestObject('Admin_Users/Page_TES Admin Portal/a_Users'), 1)

		WebUI.click(findTestObject('Admin-Master Data/Page_TES Admin Portal/a_Users'))

		System.out.println("I am in Admin User")
	}

	@When("I click on Add new User button")
	def navigate_To_New_User() {
		WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Add New User'))
		System.out.println("I am in new User creation screen")
	}

	@And("I enter (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")
	def enter_mandatory_fields(String UserName, String FirstName, String LastName, String EmailAddress, String CountryCode, String MobileNumber, String Password, String ConfirmPassword, String Language, String UserRoles) {
		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__username'), UserName)

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__firstName'), FirstName)

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__lastName'), LastName)

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__email'), EmailAddress)

		WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/svg_Select..._css-19bqh2r'))

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input_Country Code'), CountryCode)

		WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/div_91-India'))

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input_Mobile Number'), MobileNumber)

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__password'), Password)

		WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__confirmPassword'), ConfirmPassword)

		WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/span_No'))

		WebUI.selectOptionByLabel(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/select_Language'), Language, false, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.selectOptionByLabel(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/select_User Roles'), UserRoles, false, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Assign Roles'))
	}

	@When("I click on Save button")
	def click_save_button() {
		WebUI.click(findTestObject('Object Repository/Admin Users/Page_TES Admin Portal/button_Save'))
		WebUI.delay(3);
	}

	@Then("The User will be created")
	def verifyNewUser() {
		WebUI.verifyTextPresent('TestUsers', false)
		WebUI.closeBrowser()
	}
}