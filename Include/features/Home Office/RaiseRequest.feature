@tag
Feature: Raise Request

  Background: 
    Given I am in Request Summary
    When I click on Raise Request

  @Regression
  Scenario: Test validation messages of all fields
    And I click on Submit button
    Then I verify all validation messages

  Scenario: Raise Packing Request
    And Select Request Type "Packing Request"
    And Select Equipment Type
    And Select Brand Name
    And Enter name address mobile number and Email
    And Select checkbox "Returns Program Terms and Conditions"
    And Select checkbox "UPS shipping Terms and Conditions"
    And Select checkbox "Data Protection"
    And click Submit button
    Then verify Current Status "Approved"

  Scenario: Raise Manual with Carrier Request
    And Select Request Type "Process with prepaid label (Manual with Carrier)"
    And Select Equipment Type
    And Select Brand Name
    And Enter name address mobile number and Email
    And Select checkbox "Returns Program Terms and Conditions"
    And Select checkbox "UPS shipping Terms and Conditions"
    And Select checkbox "Data Protection"
    And click Submit button
    Then verify Current Status "Approved"

  Scenario: Raise Auto Request
    And Select Request Type "Process with prepaid label (Auto)"
    And Select Equipment Type
    And Select Brand Name
    And Enter name address mobile number and Email
    And Select checkbox "Returns Program Terms and Conditions"
    And Select checkbox "UPS shipping Terms and Conditions"
    And Select checkbox "Data Protection"
    And click Submit button
    Then verify Current Status "Date Assigned" 
    And verify Tracking Number
    And verify Carrier
    
  
    
    
