Feature: Test Sanity

  Background: 
    Given I open the Browser and navigate to the URL

  @Sanity
  Scenario: Test Valid Login
    When I enter Valid Login Email
    And Click Request Login OTP
    Then verify text Your OTP is sent to your email successfully.
    And Enter the OTP and accept Privacy Policy
    When click Login button
    Then verify Username
    When I Click Username
    And Click Logout
    Then verify title as Login

  @Sanity
  Scenario: Test Invalid login
    When I enter Invalid Login Email
    And Click Request Login OTP
    Then I verify text Your not authorized user.Contact your Admin.
    When I enter Valid Login Email
    And Click Request Login OTP
    Then verify text Your OTP is sent to your email successfully.

  @Sanity
  Scenario: Test Resend OTP
    When I enter Valid Login Email
    And Click Request Login OTP
    And wait Request timeout
    And Enter the Invalid OTP and accept Privacy Policy
    Then verify text OTP expired, click on Resend OTP to generate new OTP
    When Click Resend OTP button
    Then verify text Your OTP is sent to your email successfully.

  @Sanity
  Scenario: Test Invalid OTP
    When I enter Valid Login Email
    And Click Request Login OTP
    Then verify text Your OTP is sent to your email successfully.
    And Enter the Invalid OTP and accept Privacy Policy
    When click Login button
    Then verify text as Invalid OTP, Please enter valid OTP
    And Enter the OTP and accept Privacy Policy
    When click Login button
    Then verify Username
