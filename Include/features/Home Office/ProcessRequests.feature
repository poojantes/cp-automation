Feature: Processing Requests

  @Regression
  Scenario: Process Packing Request
    Given I login to Admin 
    And click on Collection Requests
    When I select Packing Request to edit
    Then I verify Request Details
    And I verify Request Status
    When I update Epicor Order Number
    And click update button
    And add "Packing Initiated" Comments
    And select Packing Initiated date
