@AdminUserCreation
Feature: Admin User Creation feature
  I want to create Admin User with valid and Invalid credentials

  @Valid
  Scenario Outline: Create Admin User with Valid credentials
    Given I navigate to Admin Users
    When I click on Add new User button
    And I enter <UserName>, <FirstName>, <LastName>, <EmailAddress>, <CountryCode>, <MobileNumber>, <Password>, <ConfirmPassword>, <Language>, <UserRoles>
    When I click on Save button
    Then The User will be created

      Examples:
      | UserName  | FirstName | LastName | EmailAddress          | CountryCode | MobileNumber | Password  | ConfirmPassword | Language | UserRoles    |
      | TestUsers | Test      | User     | eshadaipule@gmail.com | +91         | 9090909090   | Abc@12345 | Abc@12345       | English  | Client Admin |

