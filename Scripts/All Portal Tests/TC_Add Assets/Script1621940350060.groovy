import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Page_TES Admin Portal/a_All Portal'))

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/span_AWS Portal India_tes-edit'))

WebUI.click(findTestObject('Page_TES Admin Portal/a_Assets'))

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/span_Add New_lnr-file-add'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/select_Select Assets ADF Kit  All-in-one  A_739b8c'), 
    '251', true)

WebUI.setText(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/input__assetName'), 'TAT')

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/button_Add'))

