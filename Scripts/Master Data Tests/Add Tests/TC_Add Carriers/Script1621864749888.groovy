import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Master Data'))

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Carriers'))

WebUI.click(findTestObject('Admin-Master Data/Page_TES Admin Portal/button_AddNew'))

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input__carriersName'), 'TEST CARRIER NAME')

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_Street'), 'Royal Street')

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_House Number'), '5238')

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_Street1'), 'Brown Street')

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_Postal Code'), '90909')

WebUI.setText(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_City_city'), 'NUZVID')

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/input_City Add'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/select_Country'), 'India', 
    true)

WebUI.scrollToElement(findTestObject('Admin-Master Data/Page_TES Admin Portal/button_Add Carriers'), 30000)

WebUI.click(findTestObject('Admin-Master Data/Page_TES Admin Portal/button_Add Carriers'))

WebUI.verifyTextPresent('New Carrier is added Successfully!', false)

