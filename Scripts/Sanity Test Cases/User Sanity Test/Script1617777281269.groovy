import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('User Portal/Page_Welcome  User Portal/a_Raise Request'))

WebUI.click(findTestObject('User Portal/Page_Welcome  User Portal/button_Close'))

WebUI.click(findTestObject('Object Repository/User Portal/Page_Welcome  User Portal/a_Groups'))

WebUI.delay(1)

WebUI.verifyTextPresent('Group list', false)

WebUI.click(findTestObject('Object Repository/User Portal/Page_Welcome  User Portal/a_Users'))

WebUI.delay(1)

WebUI.verifyTextPresent('User Management', false)

WebUI.click(findTestObject('Object Repository/User Portal/Page_Welcome  User Portal/span_Reports'))

WebUI.delay(1)

WebUI.verifyTextPresent('Reports Search', false)

WebUI.click(findTestObject('Object Repository/User Portal/Page_Welcome  User Portal/span_Portal Docs'))

WebUI.delay(1)

WebUI.verifyTextPresent('Portal Documents', false)

