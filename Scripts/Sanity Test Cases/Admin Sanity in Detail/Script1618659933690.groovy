import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/a_Collection Requests'))

/*
WebUI.verifyTextPresent('Collection Requests', false)

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/Filters_angle-up'))

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/Filters_angle-down'))

WebUI.click(findTestObject('Admin_User Feedback/Page_TES Admin Portal/a_User Feedback'))

WebUI.verifyTextPresent('User Feedback', false)

WebUI.click(findTestObject('Admin_User Feedback/Page_TES Admin Portal/button_View Feedback'))

WebUI.verifyTextPresent('User FeedBack Details', false)

*/
WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_All Portal'))

WebUI.delay(1)

WebUI.verifyTextPresent('Portal List', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_AWS Portal'))

WebUI.delay(3)

WebUI.verifyTextPresent('Portal Details', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Portal Store'))

WebUI.verifyTextPresent('Portal Storage Configuration', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Request Types'))

WebUI.verifyTextPresent('Portal Request Types', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Request Type'))

WebUI.verifyTextPresent('Edit Request Type', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Process'))

WebUI.verifyTextPresent('Process', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Additional Fields'))

WebUI.verifyTextPresent('Additional Fields', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Additional Fields'))

WebUI.verifyTextPresent('Edit Additional Fields', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Additional Fields'))

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Manage Options'))

WebUI.verifyTextPresent('Manage Options', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Assets'))

WebUI.verifyTextPresent('Assets', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Assets'))

WebUI.verifyTextPresent('Update Portal Asset Types', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Assets'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Assets'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Assets'))

WebUI.verifyTextPresent('Add Portal Asset Types', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Assets'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/a_Portal Store'), 1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Processing Partners'))

WebUI.verifyTextPresent('Processing Partners', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Processing Partner'))

WebUI.verifyTextPresent('Edit Processing Partner', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Processing Partner'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Processing Partner'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Processing Partner'))

WebUI.verifyTextPresent('Add Processing Partner', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Processing Partner'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/a_Request Types'), 1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Collection Addresses'))

WebUI.verifyTextPresent('Collection Addresses', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Collection Addresses'))

WebUI.verifyTextPresent('Edit Collection Address', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add Collection'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add Collection'))

WebUI.verifyTextPresent('Add New Collection Address', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/a_Process'), 1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Portal User Groups'))

WebUI.verifyTextPresent('Group list', false)

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Group List'))

WebUI.verifyTextPresent('Update Group', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Create User'))

WebUI.verifyTextPresent('Create New User', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'))

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Portal Users'))

WebUI.verifyTextPresent('Portal Users', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Portal Users'))

WebUI.verifyTextPresent('Edit User', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'))

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New User'))

WebUI.verifyTextPresent('Create New User', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel New User'))

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Boxes'))

WebUI.verifyTextPresent('Boxes', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/a_Document Types'), 1)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Carriers'))

WebUI.verifyTextPresent('Portal Carriers', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Countries'))

WebUI.verifyTextPresent('Countries', false)

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Country'))

WebUI.verifyTextPresent('Edit Countries', false)

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Countries'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Countries'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Countries'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Countries'))

WebUI.verifyTextPresent('Add Countries', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Countries'))

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Labels'))

WebUI.verifyTextPresent('Labels', false)

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Packing Types'))

WebUI.verifyTextPresent('Packing Type', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Document Types'))

WebUI.verifyTextPresent('Document Types', false)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Additional Info'))

WebUI.verifyTextPresent('Additional Info', false)

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Additional Info'))

WebUI.verifyTextPresent('Add Portal Additional Info', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Additional Info'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_Report Modules'))

WebUI.verifyTextPresent('Module list', false)

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Module List'))

WebUI.verifyTextPresent('Edit Module', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Module'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Module'))

WebUI.verifyTextPresent('Create Module', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel Module'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/a_Feedback Questions'))

WebUI.verifyTextPresent('User Feedback Questions', false)

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Edit Feedback Question'))

WebUI.verifyTextPresent('Edit Feedback Question', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.scrollToElement(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Question'), 1)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Add New Question'))

WebUI.verifyTextPresent('Add Feedback Question', false)

WebUI.click(findTestObject('Admin-All Portal/Page_TES Admin Portal/button_Cancel'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Object Repository/Admin-All Portal/Page_TES Admin Portal/a_User Logs'))

WebUI.verifyTextPresent('User Logs', false)

WebUI.scrollToElement(findTestObject('Admin-Reports/Page_TES Admin Portal/a_Reports'), 1)

WebUI.click(findTestObject('Object Repository/Admin-Reports/Page_TES Admin Portal/a_Reports'))

WebUI.click(findTestObject('Object Repository/Admin-Reports/Page_TES Admin Portal/a_Portal Reports'))

WebUI.verifyTextPresent('Reports', false)

WebUI.click(findTestObject('Object Repository/Admin-Reports/Page_TES Admin Portal/a_Credit Note'))

WebUI.verifyTextPresent('Credit Note', false)

WebUI.click(findTestObject('Object Repository/Admin-Reports/Page_TES Admin Portal/a_Claims'))

WebUI.verifyTextPresent('Claims', false)

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_All Clients'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/button_AWS Portal-INDIA_1'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Language'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Processing Partners'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Packings'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Assets'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Add Services'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Add PO'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Add Terms of Use'))

WebUI.click(findTestObject('Object Repository/Admin-All Clients/Page_TES Admin Portal/a_Add Privacy Policy'))

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Master Data'))

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Additional Info'))

WebUI.verifyTextPresent('Additional Info Name', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Asset Types'))

WebUI.verifyTextPresent('Assets', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Boxes'))

WebUI.verifyTextPresent('Boxes', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Carriers'))

WebUI.verifyTextPresent('Carriers', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Countries'))

WebUI.verifyTextPresent('Countries', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Currencies'))

WebUI.verifyTextPresent('Currency', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Document Types'))

WebUI.verifyTextPresent('Document Types', false)

WebUI.scrollToElement(findTestObject('Admin-Master Data/Page_TES Admin Portal/a_User Logs'), 1)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Labels'))

WebUI.verifyTextPresent('Labels', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Languages'))

WebUI.verifyTextPresent('Languages', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Packing Lists'))

WebUI.verifyTextPresent('Packing List', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Regions'))

WebUI.verifyTextPresent('Region', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Processing Partners'))

WebUI.verifyTextPresent('Processing Partners', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Release Notes'))

WebUI.verifyTextPresent('Release Notes', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Request Types'))

WebUI.verifyTextPresent('Request Types', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Roles'))

WebUI.verifyTextPresent('Roles', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Service Category'))

WebUI.verifyTextPresent('Service Category', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Stage Process'))

WebUI.verifyTextPresent('Stage Process', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Truck List'))

WebUI.verifyTextPresent('Truck List', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Users'))

WebUI.verifyTextPresent('Users', false)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_User Logs'))

WebUI.verifyTextPresent('User Logs', false)

WebUI.closeBrowser()

