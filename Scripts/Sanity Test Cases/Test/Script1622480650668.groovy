import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sit-admin-portal.tes-amm.com/dashboard/basic')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/span_Raise Request'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/select_-- Select --TES Pack and Ship Reques_a0fa8b'), 
    '5', true)

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.setText(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/input_Please add Customer PO number_Customer Po'), 
    '111')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/svg_-- Select --_css-19bqh2r'))

WebUI.setText(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/input_Country_react-select-14-input'), 
    'India')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/div_India'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/div_option AWS India2 focused, 2 of 26. 26 _2bf502'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/div_option -- Select -- focused, 1 of 26. 2_dae51a'))

WebUI.setText(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/input__react-select-15-input'), 
    'AWS India')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/div_AWS India2'))

