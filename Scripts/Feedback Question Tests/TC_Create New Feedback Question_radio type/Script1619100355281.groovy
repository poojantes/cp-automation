import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin-Feedback Questions/Page_TES Admin Portal/a_All Portal'))

WebUI.click(findTestObject('Admin-Feedback Questions/Page_TES Admin Portal/button_Edit AWS Portal India'))

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Object Repository/Admin-Feedback Questions/Page_TES Admin Portal/a_Feedback Questions'))

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Object Repository/Admin-Feedback Questions/Page_TES Admin Portal/button_Add New Question'))

WebUI.setText(findTestObject('Object Repository/Admin-Feedback Questions/Page_TES Admin Portal/input__question'), 'New Feedback Radio')

WebUI.selectOptionByValue(findTestObject('Admin-Feedback Questions/Page_TES Admin Portal/select_input type'), 'radio', true)

WebUI.setText(findTestObject('Admin-Feedback Questions/Page_TES Admin Portal/input_radio input list'), 'New, Existing')

WebUI.click(findTestObject('Object Repository/Admin-Feedback Questions/Page_TES Admin Portal/button_Add Question'))

WebUI.delay(2)

WebUI.scrollToPosition(1, 30000)

WebUI.verifyTextPresent('New Feedback question Added Successfully', false)

WebUI.closeBrowser()

