import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/a_Collection Requests'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/i_Approved_lnr-pencil'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span__lnr-pencil'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/input__jobDescriptionNumber'), '345')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span__tes-save'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/textarea_New picked date added'), 'New picked date added')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span_Assign Pickup Date_lnr-calendar-full'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/td_2'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/select_-- Select --BluedartDHLFedExUPSTNTte_57993a'), 
    '23', true)

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Assign Pickup'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/textarea_Item collected on pick up date'), 
    'Item collected on pick up date')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Item Collected'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/textarea_Reached Facility toda'), 'Reached Facility toda')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span_Assign Pickup Date_lnr-calendar-full'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/td_2'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/input_Received by_receivedBy'), 'TES USER')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Reached Facility'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/textarea_Processing Done'), 'Processing Done')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span_Processing End Date_lnr-calendar-full'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/td_5'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/button_Done'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span_Internal Notes_lnr-pencil'))

WebUI.setText(findTestObject('null'), 
    'Processing of Raised Request successfully done')

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/span_Processing of Raised Request successfu_85d416'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_TES Admin Portal/a_Back'))

