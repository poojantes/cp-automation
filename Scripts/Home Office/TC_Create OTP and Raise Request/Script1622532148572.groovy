import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sit-portal.tes-amm.com/home-customer/login?pId=1&cId=1')

WebUI.setText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input_Login_email'), 'esha.daipule@gdc.tes-amm.com')

WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Request Login OTP'))

WebUI.delay(1)

WebUI.verifyTextPresent('Your OTP is send to your email successfully', false)

WebUI.delay(25)

//WebUI.setEncryptedText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input_Login_otp'), 'fvjo1MCnSQg=')
WebUI.click(findTestObject('Home Office/Page_AWS Portal/input_Login'))

WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Login'))

WebUI.delay(10)

/*
string url = WebUI.getUrl()

if (url == 'https://sit-portal.tes-amm.com/home-customer/request-summary/?pId=1&cId=1') {
    WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Raise Request'))
}
*/
WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Raise Request'))

WebUI.selectOptionByLabel(findTestObject('Home Office/Page_AWS Portal/select_Request Type'), 'Process with prepaid label (Manual with Carrier)', 
    true)

WebUI.selectOptionByLabel(findTestObject('Home Office/Page_AWS Portal/select_Equipment Type'), 'Laptop', true)

WebUI.selectOptionByLabel(findTestObject('Home Office/Page_AWS Portal/select_Brand'), 'Microsoft', false)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input__surname'), 'Daipule')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input__first_name'), 'Esha')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input__street'), 'Comfort Colony')

WebUI.setText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input__house_number'), 'D(1)')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input_Mobile number'), '9090909090')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input_Town'), 'Davorlim')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input__user_email'), 'esha.daipule@gdc.tes-amm.com')

WebUI.setText(findTestObject('Home Office/Page_AWS Portal/input__post_code'), '403602')

WebUI.click(findTestObject('Home Office/Page_AWS Portal/select_Country'))

WebUI.delay(2)

WebUI.check(findTestObject('Home Office/Page_AWS Portal/input_Input program terms condition'))

WebUI.check(findTestObject('Home Office/Page_AWS Portal/input_UPS shipping terms'))

WebUI.check(findTestObject('Home Office/Page_AWS Portal/input_data protection terms'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Home Office/Page_AWS Portal/select_080009001000110013001400150016001700_558924'), 
    '10:00', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Home Office/Page_AWS Portal/select_080009001000110013001400150016001700_558924_1'), 
    '16:00', true)

WebUI.click(findTestObject('Object Repository/Home Office/Page_AWS Portal/button_Submit'))

