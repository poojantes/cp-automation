import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sit-portal.tes-amm.com/home-customer/login?pId=1&cId=1')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input_Login_email'), 'esha.daipule@gdc.tes-amm.com')

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/button_Request Login OTP'))

WebUI.delay(1)

WebUI.verifyTextPresent('Your OTP is send to your email successfully', false)

WebUI.delay(25)

//WebUI.setEncryptedText(findTestObject('Object Repository/Home Office/Page_AWS Portal/input_Login_otp'), 'fvjo1MCnSQg=')
WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input_Login'))

WebUI.click(findTestObject('Homeoffice to delete/Page_AWS Portal/button_Login'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/button_Raise Request'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/select_---Select Request Type---Prepaid lab_5b195c'), 
    '63', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/select_-- Select Equipment Type --Accessori_ce12eb'), 
    'Laptop', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/select_-- Select Brand --MicrosoftGoogleOther'), 
    'Google', true)

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__surname'), 'Daipule')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__street'), 'Road')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__first_name'), 'Esha')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__house_number'), 'Number')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__user_number'), '9090909090')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__city'), 'city')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__user_email'), 'esha.daipule@gmail.com')

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__post_code'), '909')

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_-- Select Country --'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_Canada'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_Canada_1'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_-- Select Country --_1'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_-- Select Country --'))

WebUI.setText(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input__react-select-5-input'), 'Canada')

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/div_Canada'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/button_Submit'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input_I have read and agree to the_is_progr_e63c6c'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input_I have read theand accept these_is_up_bd577e'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/input_I have read theand I agree to them_is_875b47'))

WebUI.click(findTestObject('Object Repository/Homeoffice to delete/Page_AWS Portal/button_Submit'))

