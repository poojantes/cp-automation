import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_Raise Request'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_-- Select --TES Pack and Ship Reques_a0fa8b'), 
    '17', true)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.setText(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Please add Customer PO number_Customer Po'), 
    '123')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.delay(2)

WebUI.click(findTestObject('New RAISED REQUEST/Page_Welcome  User Portal/div_-- Select --'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('New RAISED REQUEST/Page_Welcome  User Portal/div_India 2'), 'India')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('New RAISED REQUEST/Page_Welcome  User Portal/div_India 2'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(3)

//WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_India'))
WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_option AWS India2 focused, 2 of 26. 26 _2bf502'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_AWS India2'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_Assets By Quantity'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_-- Select --ADF KITAll-in-oneAssBeze_33d804'), 
    '47', true)

WebUI.setText(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input__form-control'), '2')

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Done'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_-- Select --ITAD Processing'), 
    '2', true)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_-- Select --BluedartDHLFedexTest6tes_c594fc'), 
    '25', true)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_No'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_No'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_No'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Submit'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Submit'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/label_Please let us know what you think abo_f471f9'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Did we meet your expectations_Did we _9426b7'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Did we meet your expectations_Did we _9426b7'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Was it easy to purchase your item or _d27a87'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_--Select--NewHighMediumLess'), 
    'High', true)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/label_How could we have exceeded your expec_7638ba'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/label_Is there anything else you would like_baf2f4'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/label_How easy to raise request_fa fa-star'))

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Submit'))

