import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Master Data'))

WebUI.scrollToElement(findTestObject('Admin_Users/Page_TES Admin Portal/a_Users'), 1)

WebUI.click(findTestObject('Admin-Master Data/Page_TES Admin Portal/a_Users'))

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Add New User'))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__username'), 'TestUser')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__firstName'), 'TEST')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__lastName'), 'User')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__email'), 'eshadaipule@gmail.com')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input_Country Code'), '+91')

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/div_91-India'))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input_Mobile Number'), '9090909090')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__password'), 'Abc@12345')

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__confirmPassword'), 'Abc@12345')

//WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/span_No'))
WebUI.selectOptionByLabel(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/select_Language'), 'English', 
    false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/select_User Roles'), 'TES Admin', 
    false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Assign Roles'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Admin_Users/Page_TES Admin Portal/button_Save'))

WebUI.delay(3)

