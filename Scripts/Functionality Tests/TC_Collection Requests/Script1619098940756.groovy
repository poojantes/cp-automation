import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/a_Collection Requests'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Select Countries'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Created To'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Created From'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Request Id'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_City'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Add Filters'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Job Des. Number'))

WebUI.setText(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/input_RequestID'), '11')

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/div_Search'))

WebUI.setText(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/input_Created To_searchFilterName'), 
    'New Filter')

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/span_Save Filter'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/div_Filter Saved successfully'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/input_Created To_searchFilterName'))

WebUI.setText(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/input_Created To_searchFilterName'), 
    'Test Filter')

WebUI.click(findTestObject('Admin-Collection Requests/Page_TES Admin Portal/span_Save Filter'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/div_Search Name Already Exists'))

WebUI.click(findTestObject('Object Repository/Admin-Collection Requests/Page_TES Admin Portal/button_Clear Filters'))

WebUI.closeBrowser()

