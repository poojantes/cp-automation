import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin-Master Data/Page_TES Admin Portal/a_Master Data'))

WebUI.scrollToElement(findTestObject('Admin_Users/Page_TES Admin Portal/a_Users'), 1)

WebUI.click(findTestObject('Admin-Master Data/Page_TES Admin Portal/a_Users'))

WebUI.scrollToPosition(30000, 1)

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Add New User'))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__username'), findTestData('New Test Data').getValue(
        1, 1))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__firstName'), findTestData('New Test Data').getValue(
        2, 1))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__lastName'), findTestData('New Test Data').getValue(
        3, 1))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__email'), findTestData('New Test Data').getValue(
        4, 1))
/*
WebUI.selectOptionByLabel(findTestObject('Admin_Users/Page_TES Admin Portal/input_Country Code'), findTestData('New Test Data').getValue(
        5, 1), false)

WebUI.setText(findTestObject('Admin_Users/Page_TES Admin Portal/input_Mobile Number'), findTestData('New Test Data').getValue(
        6, 1))

*/
WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__password'), findTestData(
        'New Test Data').getValue(7, 1))

WebUI.setText(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/input__confirmPassword'), findTestData(
        'New Test Data').getValue(8, 1))

WebUI.click(findTestObject('Admin_Users/Page_TES Admin Portal/toggle_Is TES User'))

WebUI.selectOptionByLabel(findTestObject('Admin_Users/Page_TES Admin Portal/select_Language'), findTestData('New Test Data').getValue(
        9, 1), true)

WebUI.setText(findTestObject('Admin_Users/Page_TES Admin Portal/textarea_Remarks'), findTestData('New Test Data').getValue(
        10, 1))

WebUI.selectOptionByLabel(findTestObject('Admin_Users/Page_TES Admin Portal/select_User Roles'), findTestData('New Test Data').getValue(
        11, 1), true)

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Assign Roles'))

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Save'))

/*
WebUI.selectOptionByValue(findTestObject('Admin_Users/Page_TES Admin Portal/select_User Clients'), findTestData('New Test Data').getValue(
        12, 1), true)

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Add Client'))

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Back'))

WebUI.setText(findTestObject('Admin_Users/Page_TES Admin Portal/input_Search'), 'AbcTest')

WebUI.click(findTestObject('Object Repository/Admin_Users/Page_TES Admin Portal/button_Search'))
*/
