import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/a_Collection Requests'))

WebUI.verifyTextPresent('Collection Requests', false)

WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/button_Edit Request'))

WebUI.delay(3)

WebUI.verifyTextPresent('Quotation Requested By Client', false)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input__EpicorOrderNumber'), '345')

WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/input__EpicorOrderNumber'))

//WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/i__checkmarkCircle'))
WebUI.scrollToPosition(1, 30000)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Asset Tagging'), '100')

WebUI.scrollToPosition(1, 30000)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/textarea_Message to Client'), 'New Message to Client sent')

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/textarea__Message Regarding the Quote'), 'New Quote sent to the Client')

WebUI.selectOptionByIndex(findTestObject('Request Types/Page_TES Admin Portal/select_Quote document type'), '1')

WebUI.uploadFile(findTestObject('Request Types/Page_TES Admin Portal/upload_QuoteDocuments'), 'D:\\Office Files\\Customer Portal\\Testing Reports and Documents\\Booking Report 2021-03-02 00003051.xlsx')

WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/button_Quote Upload'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Update Quote'))

WebUI.scrollToPosition(30000, 0)

WebUI.verifyTextPresent('Quote accepted and Currently user is in the process of raising Request', false)

WebUI.closeBrowser()

