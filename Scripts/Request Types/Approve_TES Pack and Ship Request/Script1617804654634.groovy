import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/select_Edit Request'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Request Types/Page_AWS Portal/button_Approve Quote  Proceed'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Approve Quote  Proceed'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_AWS Portal/select_Packing Type'), 'Bluedart', true)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Proceed'))

WebUI.click(findTestObject('Request Types/Page_AWS Portal/toggle_Any Over Sized Assets'))

WebUI.click(findTestObject('Request Types/Page_AWS Portal/toggle_Are Assets placed at Dock'))

WebUI.setText(findTestObject('Request Types/Page_AWS Portal/textarea_Pickup Note'), 'New Pickup Note')

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Proceed_1'))

WebUI.scrollToPosition(1, 30000)

WebUI.setText(findTestObject('Request Types/Page_AWS Portal/input_Additional Email Address'), 'abc@test.com')

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Add'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Proceed_1'))

WebUI.scrollToElement(findTestObject('Request Types/Page_AWS Portal/button_Submit'), 1)

//WebUI.selectOptionByValue(findTestObject('Object Repository/Request Types/Page_AWS Portal/select_--Select List--COITransfer RequestRe_d35bdb'), 
//  '9', true)
//WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Upload'))
WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('Request Types/Page_AWS Portal/button_final Submit'))

WebUI.delay(2)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

