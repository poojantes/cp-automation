import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/a_Raise Request'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Request Type'), 'Client Packed Request', 
    true)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.setText(findTestObject('Object Repository/Request Types/Page_AWS Portal/input__Customer Po'), '123')

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select__CollectionSite'), 'TES India-AWS', 
    true)

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Security Clearance Required'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Secondary Contact'), 'John Doe')

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Country Code2'), '+91', false)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Secondary Number'), '9090909090')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Secondary Email'), 'abc@test.com')

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Assets By Quantity'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Asset Type'), 'DockStation', true)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Quantity'), '5')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Comments'), 'New Asset added')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Done'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Additional Assets with Detail'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Asset Type'), 'DockStation', true)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Manufacturer'), 'Man')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Model'), 'Mode')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Serial Number'), '123')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Asset Tag'), '567')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Cancel'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Apply Services for all assets'))

WebUI.selectOptionByIndex(findTestObject('Request Types/Page_Welcome  User Portal/select_Service Type'), '2', FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Box Count'), '2')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Pallet Count'), '1')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Pallet Weight (Weight in Kg)'), '50')

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Any Over Sized Assets'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Are Assets placed at dock'))

WebUI.setText(findTestObject('Request Types/Page_AWS Portal/textarea_Pickup Note'), 'New TES Pick up Note')

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_AWS Portal/button_Proceed_1'))

WebUI.setText(findTestObject('Request Types/Page_AWS Portal/input_Additional Email Address'), 'abc@test.com')

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Add'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_AWS Portal/button_Proceed_1'))

WebUI.scrollToElement(findTestObject('Request Types/Page_AWS Portal/button_Submit'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_AWS Portal/button_Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('Request Types/Page_AWS Portal/button_final Submit'))

WebUI.delay(2)

WebUI.closeBrowser()

