import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/span_Raise Request'))

WebUI.selectOptionByValue(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/select_-- Select --TES Pack and Ship Reques_749ce7'), 
    '17', true)

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.setText(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Please add Customer PO number_Customer Po'), 
    '123')

WebUI.delay(2)

CustomKeywords.'testJSExec.TestFirstJS.testClickObj'(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_-- Select --_css-1gtu0rj-indicatorContainer'), 10)

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))
//WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_-- Select --_css-1gtu0rj-indicatorContainer'))

//WebUI.setText(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input_Country_react-select-8-input'), 
   // 'India')

CustomKeywords.'testJSExec.TestFirstJS.testClickObj'(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_option -- Select -- focused, 1 of 26. 2_dae51a'), 10)

WebUI.delay(5)
//WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/div_option -- Select -- focused, 1 of 26. 2_dae51a'))

//WebUI.setText(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/input__react-select-9-input'), 
//    'AWS India2')

WebUI.click(findTestObject('Object Repository/New RAISED REQUEST/Page_Welcome  User Portal/button_Proceed'))

WebUI.closeBrowser()

