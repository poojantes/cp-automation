import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/Admin Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/a_Collection Requests'))

WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/button_Approved Request'))

WebUI.delay(3)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input__EpicorOrderNumber'), '345')

//WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/input__EpicorOrderNumber'))
WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/i__checkmarkCircle'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_TES Admin Portal/select_Processing Partner'), 'AWS-USA', false)

WebUI.click(findTestObject('Request Types/Page_TES Admin Portal/button_Yes'))

WebUI.scrollToElement(findTestObject('Request Types/Page_TES Admin Portal/div_Customer Reports'), 1)

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_TES Admin Portal/select_Report document type-2'), 'Reception Reports', 
    false)

WebUI.uploadFile(findTestObject('Request Types/Page_TES Admin Portal/upload_Customer Reports_2'), 'D:\\Office Files\\Customer Portal\\Testing Reports and Documents\\Reception Report 2021-03-02 000030496.xlsx')

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Upload'))

WebUI.scrollToElement(findTestObject('Request Types/Page_TES Admin Portal/button_Update'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Comments'), 'Picked Up')

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_TES Admin Portal/select_Carrier'), 'Bluedart', true)

WebUI.scrollToElement(findTestObject('Request Types/Page_TES Admin Portal/button_Assign Pickup'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Assign Pickup'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Request Types/Page_TES Admin Portal/button_Update'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Update'))

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Comments'), 'Reached Facility')

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Received by'), 'Test User')

WebUI.scrollToElement(findTestObject('Request Types/Page_TES Admin Portal/button_Reached Facility'), 1)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Reached Facility'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Update'))

WebUI.delay(3)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Comments'), 'TEST')

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Done'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Update'))

WebUI.delay(3)

WebUI.setText(findTestObject('Request Types/Page_TES Admin Portal/input_Comments'), 'Done')

WebUI.click(findTestObject('Object Repository/Request Types/Page_TES Admin Portal/button_Report Done'))

WebUI.closeBrowser()

