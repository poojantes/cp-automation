import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Base Test Cases/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)

/*
if (WebUI.verifyTextPresent('Release Notes', true)) {
    WebUI.click(findTestObject('Object Repository/General/check_Dont show Again'))
}
*/

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/a_Raise Request'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Request Type'), 'New Onsite Service Request', 
    true)

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.delay(3)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__CustomerPO'), '123')

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/checkbox_test'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select__CollectionSite'), 'TES India-AWS', 
    false)

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Security Clearance Required'))

WebUI.scrollToPosition(1, 30000)

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/textarea_Your Message to TES'), 'New Message to TES')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Apply Services for all assets'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/span_YesNo'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/span_YesNo'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/span_YesNo'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed_1'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Apply Services for all assets'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Assets By Quantity'))

WebUI.selectOptionByValue(findTestObject('Request Types/Page_Welcome  User Portal/select_Asset Type'), '59', true)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Quantity'), '5')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Done'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Additional Assets with Detail'))

WebUI.selectOptionByValue(findTestObject('Request Types/Page_Welcome  User Portal/select_Asset Type'), '59', true)

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Manufacturer'), 'Man')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input__Model'), 'Mode')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Serial Number'), '123')

WebUI.setText(findTestObject('Request Types/Page_Welcome  User Portal/input_Asset Tag'), '345')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Cancel'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.setText(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/input_Additional Email_emailAddress'), 
    'abc@test.com')

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Add'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Request Types/Page_Welcome  User Portal/toggle_Apply Services for all assets'))

WebUI.selectOptionByLabel(findTestObject('Request Types/Page_Welcome  User Portal/select_Service Type'), 'ITAD Processing', 
    true)

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Proceed'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Submit'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Submit'))

WebUI.click(findTestObject('Object Repository/Request Types/Page_Welcome  User Portal/button_Cancel'))

